#include "CryptoHelper.h"
#include "SPCrypto.h"
#include <chrono>

int main(int argc, char** argv) {
	// mode key out

	auto start_time = std::chrono::high_resolution_clock::now();

	if (argc < 5) {
	
		std::cout << "Usage: \n\t" << argv[0] << " <analyze|crypt|decrypt> <in_file> <out_file> <key_file>" << std::endl;
		return -1;
	}

	SPCrypto spCrypto = SPCrypto();

	const std::string strCryptMode = "crypt";
	const std::string strDecryptMode = "decrypt";
	const std::string strAnalyzeMode = "analyze";

	std::string argMode(argv[1]);
	std::string argInFile(argv[2]);
	std::string argOutFile(argv[3]);
	std::string argKeyFile(argv[4]);

	spCrypto.setKey(argKeyFile);

	if (argMode == strCryptMode) {
	
		spCrypto.getInFlow(argInFile);
		spCrypto.encrypt();
		spCrypto.getOutFlow(argOutFile);
	}
	else if (argMode == strDecryptMode) {
	
		spCrypto.setOutFlow(argInFile);
		spCrypto.decrypt();
		spCrypto.getDecFlow(argOutFile);
	}
	else if (argMode == strAnalyzeMode) {
		spCrypto.getInFlow(argInFile);
		spCrypto.setOutFlow(argOutFile);
		spCrypto.analyze();
	}
	else {
	
		std::cout << "Usage: " << argv[0] << "<crypt|decrypt> <in_file> <out_file> <key_file>" << std::endl;
		return -1;
	}

	auto finish_time = std::chrono::high_resolution_clock::now();
	auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(finish_time - start_time);

	std::cout << "Time consumption (ms): " << std::dec << duration.count() << std::endl;

	return 0;
}