#include "SPCrypto.h"

SPCrypto::SPCrypto(){

	cHelper = CryptoHelper();
}

void SPCrypto::analyze(){

	tstBlock = inBlocks.at(0);
	auto cmpBlock = outBlocks.at(0);

	for (CryptoHelper::key_t key = 0; key <= UINT32_MAX; key++) {
		vKS = key;
		if (encryptBlock(tstBlock) == cmpBlock) {
			
			decrypt();

			if (decBlocks == inBlocks) {
				vKS = \
					(((vKS >> 0) & 0xFF) << 24) | \
					(((vKS >> 8) & 0xFF) << 16) | \
					(((vKS >> 16) & 0xFF) << 8) | \
					(((vKS >> 24) & 0xFF) << 0);

				std::cout << "Founded key: 0x" << std::hex << vKS << std::endl;
				break;
			}
			else {
			
				decBlocks.clear();
			}
		}
	}

	return;
}

void SPCrypto::setOutFlow(std::string inFile){

	std::ifstream inData(inFile, std::ios::in | std::ios::binary);

	char c = inData.get();

	while (inData.good()) {

		outFlow.push_back(c);
		c = inData.get();
	}

	inData.close();
	outBlocks = cHelper.splitByBlocks(outFlow);
	return;
}

void SPCrypto::getInFlow(std::string inFile){
	std::ifstream inData(inFile, std::ios::in | std::ios::binary);

	char c = inData.get();

	while (inData.good()) {
	
		inFlow.push_back(c);
		c = inData.get();
	}

	inData.close();

	inBlocks = cHelper.splitByBlocks(inFlow);
}

void SPCrypto::getDecFlow(std::string decFile) {

	for (auto curBlock = decBlocks.begin(); curBlock != decBlocks.end(); curBlock++) {

		if (decBlocks.size() >= 2 && curBlock == (decBlocks.end() - 1)) {

			unsigned lastBlockLen = cHelper.getLastBlockLen();
			auto tmpBlock = CryptoHelper::block_t();

			for (unsigned i = 0; i < lastBlockLen; i++)
				tmpBlock |= (*curBlock & (0x1 << i));

			uint32_t outWord = *curBlock;

			for (unsigned i = 0; i < lastBlockLen / 8; i++)
				decFlow.push_back((outWord >> (lastBlockLen - 8 * (1 + i))) & 0xFF);

		}
		else {

			uint32_t outWord = *curBlock;
			size_t block_len = CryptoHelper::block_len;
			for (unsigned i = 0; i < block_len / 8; i++) {
				CryptoHelper::flowelem_t pushval = (outWord >> (block_len - 8 * (1 + i))) & 0xFF;
				decFlow.push_back(pushval);
			}

		}
	}

	std::ofstream decData(decFile, std::ios::out | std::ios::binary);

	for (auto elem : decFlow) {
	
		decData << elem;
	}

	decData.close();
}

void SPCrypto::getOutFlow(std::string outFile){

	for (auto curBlock = outBlocks.begin(); curBlock != outBlocks.end(); curBlock++) {
	
		if (outBlocks.size() >= 2 && curBlock == (outBlocks.end() - 1)) {
		
			unsigned lastBlockLen = cHelper.getLastBlockLen();
			auto tmpBlock = CryptoHelper::block_t();
			
			for (unsigned i = 0; i < lastBlockLen; i++)
				tmpBlock |= (*curBlock & (0x1 << i));

			uint32_t outWord = *curBlock;
			
			for (unsigned i = 0; i < lastBlockLen / 8; i++)
				outFlow.push_back((outWord >> (lastBlockLen - 8 * (1 + i))) & 0xFF);

		}
		else {
		
			uint32_t outWord = *curBlock;
			for (unsigned i = 0; i < CryptoHelper::block_len / 8; i++) {
				size_t block_len = CryptoHelper::block_len;
				CryptoHelper::flowelem_t pushval = (outWord >> (block_len - 8 * (1 + i))) & 0xFF;
				outFlow.push_back(pushval);
			}

		}
	}

	std::ofstream outData(outFile, std::ios::out | std::ios::binary);

	for (auto elem : outFlow) {

		outData << elem;
	}

	outData.close();

}

void SPCrypto::encrypt(){


	for (auto curBlock = inBlocks.begin(); curBlock != inBlocks.end(); curBlock++) {

		if (inBlocks.size() >= 2 && curBlock == (inBlocks.end() - 2)) {
		
			const auto outTMP = encryptBlock(*curBlock);
			const size_t lastBlockLen = cHelper.getLastBlockLen();
			const int fitLen = CryptoHelper::block_len - lastBlockLen;
			auto curCryptoBlock = CryptoHelper::block_t();

			for (unsigned i = 0; i < lastBlockLen; i++) {

				curCryptoBlock = *(inBlocks.end() - 1);
			}
			for (unsigned i = lastBlockLen; i < CryptoHelper::block_len; i++) {
				auto negMask = ~(0x1 << i);
				auto mask = (outTMP & (0x1 << i));

				curCryptoBlock &= negMask;
				curCryptoBlock |= mask;

			}

			outBlocks.push_back(encryptBlock(curCryptoBlock));
			outBlocks.push_back(encryptBlock(*(inBlocks.end() - 2)));
			break;
		}
		else {
		
			outBlocks.push_back(encryptBlock(*curBlock));
		}
	}
}

void SPCrypto::decrypt(){

	for (auto curBlock = outBlocks.begin(); curBlock != outBlocks.end(); curBlock++) {

		if (outBlocks.size() >= 2 && curBlock == (outBlocks.end() - 2)) {

			const auto outTMP = decryptBlock(*curBlock);
			const size_t lastBlockLen = cHelper.getLastBlockLen();
			const int fitLen = CryptoHelper::block_len - lastBlockLen;
			auto curCryptoBlock = CryptoHelper::block_t();

			for (unsigned i = 0; i < lastBlockLen; i++) {

				curCryptoBlock = *(outBlocks.end() - 1);
			}
			for (unsigned i = lastBlockLen; i < CryptoHelper::block_len; i++) {

				auto negMask = ~(0x1 << i);
				auto mask = (outTMP & (0x1 << i));

				curCryptoBlock &= negMask;
				curCryptoBlock |= mask;
			}

			decBlocks.push_back(decryptBlock(curCryptoBlock));
			decBlocks.push_back(decryptBlock(*(outBlocks.end() - 2)));
			break;
		}
		else {

			decBlocks.push_back(decryptBlock(*curBlock));
		}
	}

}

bool SPCrypto::validate(){

	bool result = true;
	size_t minSize = inFlow.size();

	if (inFlow.size() != decFlow.size())
		result = false;

	for (size_t i = 0; i < minSize; i++) {
	
		if (inFlow.at(i) != decFlow.at(i)) {
			result = false;
		}
	}

	/*if (inBlocks.size() != decBlocks.size()) {
		result = false;
		minSize = std::min(inBlocks.size(), decBlocks.size());
	}

	for (size_t i = 0; i < minSize; i++) {
	
		if (inBlocks[i] != decBlocks[i]) {
		
			std::cout << "#" << i << std::endl;
			std::cout << inBlocks[i] << std::endl;
			std::cout << decBlocks[i] << std::endl;
			std::cout << std::endl;
			result = false;
		}
	}*/



	return result;
}

CryptoHelper::block_t SPCrypto::roundSigma(CryptoHelper::block_t inBlock, CryptoHelper::key_t roundKey){

	auto outBlock = 0;
	auto outSubBlocks = CryptoHelper::sub_blocks_t();
	
	inBlock ^= roundKey;
	outBlock ^= SP0[inBlock & 0xFF];
	outBlock ^= SP1[(inBlock >> 8) & 0xFF];
	outBlock ^= SP2[(inBlock >> 16) & 0xFF];
	outBlock ^= SP3[(inBlock >> 24) & 0xFF];

	return outBlock;
}

CryptoHelper::block_t SPCrypto::roundDeSigma(CryptoHelper::block_t inBlock, CryptoHelper::key_t roundKey){
	
	CryptoHelper::block_t outBlock = inBlock;
	uint8_t 
		B0 = 0,
		B1 = 0,
		B2 = 0,
		B3 = 0;

	for (int i = 0; i < 8; i++) {
	
		B0 |= ((((outBlock >> P[i]) & 1))) << i;
		B1 |= ((((outBlock >> P[i + 8]) & 1))) << i;
		B2 |= ((((outBlock >> P[i + 16]) & 1))) << i;
		B3 |= ((((outBlock >> P[i + 24]) & 1))) << i;
	}
	
	outBlock = (deS[B0] << 0) | (deS[B1] << 8) | (deS[B2] << 16) | (deS[B3] << 24);
	outBlock ^= roundKey;

	return outBlock;
}

CryptoHelper::block_t SPCrypto::pShuffle(CryptoHelper::block_t inBlock) {

	auto outBlock = CryptoHelper::block_t();
	for (size_t i = 0; i < CryptoHelper::block_len; i++) {
		outBlock |= (inBlock & (0x01 << cHelper.P(i)));
	}

	return outBlock;
}

CryptoHelper::block_t SPCrypto::pDeShuffle(CryptoHelper::block_t inBlock) {

	auto outBlock = CryptoHelper::block_t();

	for (int i = 0; i < CryptoHelper::block_len; i++) {
		outBlock |= (inBlock & (0x01 << cHelper.deP(i)));
	}

	return outBlock;
}
